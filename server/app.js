var express = require('express');
var app = express();
var bodyParse = require("body-parser");

app.use(express.static('static'));
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extended:true}));

// 设置跨域
app.all('*',function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By","3.2.1");
  res.header("Content-Type","application/json;charset = utf-8");
  res.header("X-Frame-Options","DENY");
  res.header("X-XSS-Protection","1;mode=block");
  // res.header("Content-Security-Policy","default-src 'self'");
  next();
});

// router
app.get('/',function(req,res){
  res.sendFile("static");
});

// 接口
app.get('/api/res',function(req,res){
  res.json({
    status:200,
    msg:"数据获取成功",
    data:{
        name:"斯文的鸡蛋",
        id:1,
        age:28
    }
  });
});

// 监听
var server = app.listen(3005,function(){
  var host = server.address().address;
  var port = server.address().port;
  console.log("server listen at " + host,port);
});
