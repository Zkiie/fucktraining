// 初始化主体对象
var fuck = {};
fuck.Main = function() {};
var All = null; //所有数据
var cityList = []; //城市列表
fuck.Main.prototype = {
    Ajax: function(type, url, data, success, failed) {
        // 创建ajax对象
        var xhr = null;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else {
            xhr = new ActiveXObject('Microsoft.XMLHTTP')
        }

        var type = type.toUpperCase();
        // 用于清除缓存
        var random = Math.random();

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i].trim();
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        }
        //  数据处理
        var sess_id = getCookie('sessionid') || '';
        var params;
        var time = new Date().getTime();
        data = JSON.stringify(data);
        params = 'data=' + data + '&';
        params += 'time=' + time + '&';
        params += 'sessionid=' + sess_id + '&';
        params += 'sign=' + md5(data + time + sess_id);

        if (type == 'GET') {
            if (data) {
                xhr.open('GET', url + '?' + data, true);
            } else {
                xhr.open('GET', url + '?t=' + random, true);
            }
            xhr.send();

        } else if (type == 'POST') {
            xhr.open('POST', url, true);
            // 如果需要像 html 表单那样 POST 数据，请使用 setRequestHeader() 来添加 http 头。
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(params);
        }

        // 处理返回数据
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var data = JSON.parse(xhr.responseText);
                    success(data);
                } else {
                    if (failed) {
                        failed(xhr.status);
                    }
                }
            }
        }
    },
    Trim: function(str) {
        return str.replace(/\s/g, '');
    },
    getMsg: function(str, time) {
        var msg = document.getElementById("msg");
        msg.innerHTML = str;
        msg.style.display = "block";
        setTimeout(function() {
            msg.style.display = "none";
        }, time);
    },
    getData: function() { //获取数据
        var _this = this;
        _this.Ajax('get', '/js/db.json', {}, function(res) {
            All = res;
        }, function(res) {});
    },
    renderData: function() { //初始化页面
        var _this = this;
        var data = All.res.data;
        var post = document.getElementById("post");
        var select = document.getElementById("city");
        var btn = document.getElementById("btn");
        var selectDom = '<option value="0">请选择城市</option>';

        for (var key in data) {
            cityList.push(key);
        }

        for (var i = 0; i < cityList.length; i++) {
            selectDom += '<option value="' + (i + 1) + '">' + cityList[i] + '</option>';
        }

        // 生成下拉列表
        select.innerHTML = selectDom;

        // 提交查询
        btn.onclick = function(e) {
            _this.checkValue(); //检测输入的数据 并发起查询
        }

        // 回车查询
        post.addEventListener('keydown', function(e) {
            if (e.keyCode == 13) {
                btn.click();
            }
        }, false);

        // 城市查询
        select.addEventListener('change', function(e) {
            post.value = "";
            _this.searchData("city", _this.Trim(e.target.value));
        }, false);


    },
    checkValue: function() { //数据校验
        var value = this.Trim(post.value);
        var isValue = true;
        switch (value) {
            case "":
                console.log("查询数据不能为空!");
                this.getMsg("查询数据不能为空!", 2500);
                isValue = false;
                break;
            case "公司":
            case "有限":
            case "科技":
            case "有限科技":
            case "科技有限公司":
            case "有限公司":
            case "网络科技":
                console.log("无效的关键字!");
                this.getMsg("无效的关键字!", 2500);
                isValue = false;
                break;
        }

        if (isValue) {
            this.searchData("btn", value); //发起查询
        }

    },
    notData: function(str) { //暂无数据
        str += '<li class="clearfix odd">';
        str += '    <p class="tc">暂无数据</p>';
        str += '</li>';
        return str;
    },
    renderDom: function(key, data, city, domStr) {
        if (key % 2) {
            domStr += '<li class="clearfix odd">'
            domStr += '    <span class="left tc">' + data + '</span>'
            domStr += '    <span class="right tc">' + city + '</span>'
            domStr += '</li>'
        } else {
            domStr += '<li class="clearfix">'
            domStr += '    <span class="left tc">' + data + '</span>'
            domStr += '    <span class="right tc">' + city + '</span>'
            domStr += '</li>'
        }

        return domStr;
    },
    searchData: function(type, value) { //查询方法
        var _this = this;
        var listWrap = document.getElementById("listWrap");
        var list = document.getElementById("list");
        var data = All.res.data;
        var city = null;
        var listDom = '';

        switch (type) {
            case "btn":
                // console.log("模糊查询");
                var list = document.getElementById("list");
                var comp = _this.searchByObj(value, All);

                if (comp == "") {
                    comp = this.notData(listDom);
                }

                listWrap.innerHTML = comp;
                list.style.display = "block";
                break;
            case "city":
                if (value == 0) {
                    // console.log("不做查询");
                    listDom = this.notData(listDom);
                    listWrap.innerHTML = listDom;
                    return false;
                };
                // console.log("城市查询");
                for (var i = 0; i < cityList.length; i++) {
                    if (i == (value - 1)) {
                        city = cityList[i];
                    }

                }

                for (var key in data[city]) {
                    listDom = this.renderDom(key, data[city][key], city, listDom);
                }

                listWrap.innerHTML = listDom;
                list.style.display = "block";
                break;
        }

    },
    searchByObj: function(keyWord, obj) {
        if (!(obj instanceof Object)) {
            return false;
        }
        var data = obj.res.data;
        var len = cityList.length;
        var arr = [];
        var listDom = '';
        for (var i = 0; i < len; i++) {
            var objLen = null;
            for (var key in data[cityList[i]]) {
                objLen++;
            }

            for (var j = 0; j < objLen; j++) {
                if (data[cityList[i]][j].indexOf(keyWord) >= 0) {
                    // arr.push(data[cityList[i]][j]);
                    listDom = this.renderDom(j, data[cityList[i]][j], cityList[i], listDom);
                }
            }

        }
        // return arr;
        return listDom;
    }
};

window.onload = function() {
    var Main = new fuck.Main();
    Main.getData();
    setTimeout(function() {
        Main.renderData();
    }, 1000);
}